package com.omniture

import scala.collection.mutable.ListBuffer

case class Attribution(searchEngine: String, keyword: String, revenue: BigDecimal)

object Attribution {
  def attribute(sessionId: Long, events: Array[SessionEvent]) = {
    val sortedEvents = events.sortBy(r => r.hitTimeGmt)

    val attributions = ListBuffer[Attribution]()
    var searchEngineEvent = Option.empty[SessionEvent]
    for (event <- sortedEvents) {
      event match {
        case a if a.eventType == EventType.SEARCH_ENGINE => searchEngineEvent = Option(event)
        case a if a.eventType == EventType.PURCHASE && searchEngineEvent.isDefined => {
          val searchEvent = searchEngineEvent.get
          attributions += Attribution(searchEvent.engine, searchEvent.keyword, a.revenue)
        }
      }
    }

    attributions
  }
}
