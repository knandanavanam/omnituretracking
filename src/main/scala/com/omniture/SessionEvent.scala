package com.omniture

import java.net.URI
import java.time.{ZoneId, ZonedDateTime}
import java.time.format.DateTimeFormatter

import com.google.common.base.Charsets
import com.google.common.hash.Hashing
import org.apache.commons.lang.StringUtils
import org.apache.http.NameValuePair
import org.apache.http.client.utils.URLEncodedUtils

import collection.JavaConverters._

object EventType extends Enumeration {
  val PURCHASE, SEARCH_ENGINE = Value
}

case class SessionEvent(sessionId: Long, hitTimeGmt: BigInt, day: String, engine: String, keyword: String, revenue: BigDecimal, eventType: EventType.Value)

object SessionEvent {
  val format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneId.systemDefault())
  val dayFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd").withZone(ZoneId.systemDefault())

  def convert(event: TrackingEvent) = {
    val sessionId: Long = extractSessionId(event)
    val (searchEngine: String, query: Option[NameValuePair]) = extractHostAndQueryString(event)
    val day: String = extractDay(event)

    val eventList = Option(event.event_list).getOrElse("").split(",").filter(r => StringUtils.isNotEmpty(r)).map(r => r.toInt).toSet
    val revenue = extractRevenue(event.product_list)

    (searchEngine, query, eventList) match {
      case (a, b, _) if !StringUtils.contains(a, "INTERNAL") && b.isDefined => Option(SessionEvent(sessionId, BigInt(event.hit_time_gmt), day, a.toString, query.get.getValue, null, EventType.SEARCH_ENGINE))
      case (_, _, c) if c.contains(1) => Option(SessionEvent(sessionId, BigInt(event.hit_time_gmt), day, null, null, revenue, EventType.PURCHASE))
      case _ => Option.empty
    }
  }

  private def extractDay(event: TrackingEvent) = {
    val dateTime = ZonedDateTime.parse(event.date_time, format)
    val day = dayFormat.format(dateTime.toInstant)
    day
  }

  private def extractHostAndQueryString(event: TrackingEvent) = {
    val uri = new URI(event.referrer.toLowerCase)
    val nameValuePairs = URLEncodedUtils.parse(uri, "UTF-8").asScala
    val host = hostName(uri.getHost)
    val q = nameValuePairs.find(r => StringUtils.equals(r.getName, "q"))
    val p = nameValuePairs.find(r => StringUtils.equals(r.getName, "p"))

    (q, p, host) match {
      case (_, b, h) if h.contains("yahoo") => (h, b)
      case _ => (host, q)
    }
  }

  private def extractSessionId(event: TrackingEvent) = {
    val id = event.user_agent + "|" + event.ip
    val sessionId = Hashing.md5().hashString(id, Charsets.US_ASCII).asLong()
    sessionId
  }

  def extractRevenue(productList: String) = {
    Option(productList).getOrElse("").split(",")
      .map(r => r.split(";"))
      .map(r => ProductInfo.convertToProduct(r))
      .filter(r => r.isDefined)
      .map(r => r.get)
      .map(r => r.totalRevenue * r.numberOfItems)
      .sum
  }

  def hostName(host: String) = {
    host match {
      case a if StringUtils.contains(a, "esshopzilla") => "INTERNAL"
      case _ => host
    }
  }
}
