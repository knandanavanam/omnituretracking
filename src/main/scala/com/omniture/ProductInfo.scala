package com.omniture

case class ProductInfo(category: String, productName: String, numberOfItems: Int, totalRevenue: BigDecimal, customEvents: String, merchandizingEvars: String)

object ProductInfo {
  def convertToProduct(columns: Array[String]) = {
    if (columns.size >= 4) {
      val category = columns(0)
      val productName = columns(1)
      val numberOfItems = columns(2).toInt
      val totalRevenue = BigDecimal(columns(3))
      val customEvents = if (columns.size >= 5) columns(4) else ""
      val merchandizingEvars = if (columns.size >= 6) columns(5) else ""
      Option(ProductInfo(category, productName, numberOfItems, totalRevenue, customEvents, merchandizingEvars))
    }
    else {
      Option.empty
    }
  }
}
