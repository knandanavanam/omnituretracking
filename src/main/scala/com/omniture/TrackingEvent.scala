package com.omniture

case class TrackingEvent(hit_time_gmt: String, date_time: String, user_agent: String, ip: String, event_list: String, geo_city: String, geo_region: String, geo_country: String, pagename: String, page_url: String, product_list: String, referrer: String)