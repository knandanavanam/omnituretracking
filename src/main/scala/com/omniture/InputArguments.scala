package com.omniture

import org.rogach.scallop.ScallopConf

class InputArguments(args: Array[String]) extends ScallopConf(args) {
  banner("""Application Arguments --app-name AppName --cluster local[*] --input-dir /directory --output-dir /directory""")

  val appName = opt[String](name = "app-name", required = true)
  val cluster = opt[String](name = "cluster", required = false)
  val input = opt[String](name = "input-dir", required = true)
  val output = opt[String](name = "output-bucket", required = true)

  override def onError(e: Throwable) = {
    builder.printHelp()
    throw e
  }
}
