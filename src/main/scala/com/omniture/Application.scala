package com.omniture

import java.time.ZonedDateTime

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import ObjectEncoders._

object Application {
  def main(args: Array[String]) = {
    val arguments = new InputArguments(args)
    arguments.verify()

    val conf = new SparkConf()
      .setAppName(arguments.appName())

    if (arguments.cluster.isDefined) {
      conf.setMaster(arguments.cluster())
    }

    val spark = SparkSession.builder()
      .config(conf)
      .getOrCreate()

    import spark.implicits._

    val localDate = ZonedDateTime.now().toLocalDate.toString

    val data = spark.read
      .format("csv")
      .option("header", "true")
      .option("delimiter", "\t")
      .load(arguments.input())
      .as[TrackingEvent]
      .filter(r => r.ip != null && r.user_agent != null)
      .map(r => SessionEvent.convert(r).orNull)
      .filter(r => r != null)
      .filter(r => EventType.PURCHASE == r.eventType || EventType.SEARCH_ENGINE == r.eventType)

    val searchEngineRevenues = data.groupByKey(r => r.sessionId)
      .flatMapGroups((key, values) => Attribution.attribute(key, values.toArray))

    val searchEngineAttributions = searchEngineRevenues.groupBy("searchEngine", "keyword")
      .agg(sum("revenue").alias("tot_revenue"))
      .coalesce(1)
      .orderBy(desc("tot_revenue"))
      .toDF("search_engine_domain", "search_keyword", "revenue")

    searchEngineAttributions.write
      .format("csv")
      .option("header", "true")
      .option("delimiter", "\t")
      .save(s"""${arguments.output()}/${localDate}_SearchKeywordPerformance""")

    spark.close()
  }
}
