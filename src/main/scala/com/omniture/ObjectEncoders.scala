package com.omniture

object ObjectEncoders {
  implicit def sessionEventEncoder: org.apache.spark.sql.Encoder[SessionEvent] = org.apache.spark.sql.Encoders.kryo[SessionEvent]
}
