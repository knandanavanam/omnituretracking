#!/usr/bin/env bash
cd ~/OmnitureTracking
aws s3 cp s3://omniture-tracking/data.sql .

export APP_JARS_CLASSPATH=$(find lib/*.jar | paste -sd,)
echo $APP_JARS_CLASSPATH

spark-submit --class com.omniture.Application \
--deploy-mode client \
--jars ${APP_JARS_CLASSPATH} \
--conf spark.driver.memory=2G \
--conf spark.executor.memory=2G \
lib/OmnitureTracking-0.1-SNAPSHOT.jar \
--app-name OmnitureTracking \
--cluster local \
--input-dir file:///home/hadoop/OmnitureTracking/data.sql \
--output-bucket file:///home/hadoop/OmnitureTracking