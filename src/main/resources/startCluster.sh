#!/usr/bin/env bash
cd ~/OmnitureTracking
export APP_JARS_CLASSPATH=$(find lib/*.jar | paste -sd,)
echo $APP_JARS_CLASSPATH

spark-submit --class com.omniture.Application \
--deploy-mode cluster \
--jars ${APP_JARS_CLASSPATH} \
--conf spark.driver.memory=2G \
--conf spark.executor.memory=2G \
lib/OmnitureTracking-0.1-SNAPSHOT.jar \
--app-name OmnitureTracking \
--input-dir s3://omniture-tracking/data.sql \
--output-bucket s3://omniture-tracking
