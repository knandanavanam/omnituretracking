package com.omniture

import java.io.File

import org.apache.commons.io.FileUtils
import org.scalatest.FunSpec

class ApplicationTest extends FunSpec {
  describe("ApplicationTest") {
    val input = classOf[ApplicationTest].getResource("/data.sql").toURI.toURL.getPath
    val output = "/tmp/output"

    FileUtils.deleteDirectory(new File(output))

    val args = Array("--app-name", "OmnitureTracking",
      "--cluster", "local[*]",
      "--input-dir", input,
      "--output-bucket", output)

    Application.main(args)
  }
}
