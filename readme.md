### OmnitureTracking Application

##### High-Level explanation Steps to run OmnitureTracking Application:
- Clone the project
- Build application: "mvn clean package" will create tar.gz file under target/ folder 
- GOTO AWS Console
- Create S3 Bucket named "omniture-tracking" for loading input data + App tar.gz package
- Create EMR cluster that has Spark capability
- Add SSH Access permissions to access EMR master
- Copy Input File and tar.gz package to "omniture-tracking" S3 bucket
- SSH to EMR cluster using .pem file used while creating the EMR cluster
- Download tar.gz package to Master using command: aws s3 cp s3://omniture-tracking/OmnitureTracking-0.1-SNAPSHOT-bin.tar.gz .
- Create OmnitureTracking directory in user home directory
- Copy the unpacked .tar.gz files to OmnitureTracking directory: "cd OmnitureTracking" and "tar xvzf ../OmnitureTracking-0.1-SNAPSHOT-bin.tar.gz" 
- If you want to run locally, copy input-file to local using command: 
    - "aws s3 cp s3://omniture-tracking/data.sql ." and "sh startLocal.sh" 
    -It creates output in file:///home/hadoop/OmnitureTracking/yyyy-MM-dd_SearchKeywordPerformance Directory
- If you want to run in cluster:
    - "sh startCluster.sh" and wait until it finishes
    - You will see the output in s3://omniture-tracking/yyyy-MM-dd_SearchKeywordPerformance Directory   

##### Technologies Used:
- Maven for building project
- Spark 2.4.0
- Java 1.8 with Scala 2.11.12
- Scallop API for reading input arguments, ScalaTest framework for testing the app locally
- Intellij IDE
- GIT for version control of code
- AWS EMR, S3 for executing the application

Please kindly let me know, if you have any questions.

#### Assumptions when solving the problem:
- Should we consider IP address or combination of columns as the session key to group the events?
    - Data points that seemed appropriate are IP_Address and UserAgent. Although, these are not good data points in real world. 
- Can referrer be anything and not limited to google, Bing, yahoo and Internal search?
    - Used host address as the Search Engine identifier. Given that, it should work for all search engine types.
- Should we attribute to Search Engine, only when the first request of the session is from Search or it can be anywhere before cart adds and purchase?
    - Attributing to latest possible Search Engine, if there is any, to the order happened after Search Engine event. Only considered Search and Purchase events for attribution.
- Whom should we attribute if more than one search engine referrers are available with in the session?
    - Pick the latest possible Search Engine event, if there is any.
- If there are more than one purchase. Should we attribute all purchases to the Search engine?
    - Attributing to all purchases with in the session makes sense. Although, Matt mentioned that there is only one order per session. Code works both ways.
- Shall I attribute only when the product is added after search and the query matches with the name of the product?
    - Ignored this assumption because Attribution is done if the order is made, irrespective of whether they search for the specific product or not. They have come to our site from search engine and made a purchase.    
- Shall I assume that every search engine contains the same query parameter q as the search keyword?
    - yahoo seems to have a different query parameter p. Implemented specific case for Yahoo. Rest all will fall back to query parameter q.

#### Application Logic:
- Load TSV File and convert to Java Object TrackingEvent
- Convert TrackingEvent to SessionEvent (Either Purchase or Search-Engine Event only)
- Create SessionId based on Md5 hash of (IP_ADDRESS and UserAgent)
- Filter to only Purchases and Search-Engine Events (Prevent from leaking more events, if we extend SessionEvent Transformer)
- Group by sessionId
- With in each session group, order the requests by hitTimeGmt asc and apply window logic to attribute search engine to orders
- Group by search-engine to calculate to sum of revenue (Total attribution)
- Coalesce (repartition to 1 file) before sorting to create only one file in the output directory
- Order the set of records in the descending order of total_revenue column
- Write the output as TSV file to output directory with Header

Step-By-Step Deploy Steps:
- Create Key Pair

    - ![](img/1.createKeyPair.png)

- Key Pair Created

    - ![](img/2.KeyPairCreated.png)

- Create EMR Spark Cluster

    - ![](img/3.CreateEMRCluster.png)
    
- Building EMR Spark Cluster

    - ![](img/4.BuildingEMRCluster.png)
    
- EMR Spark Cluster Created and Waiting for Requests

    - ![](img/5.EMRCreatedAndWaiting.png)
    
- Add SSH Access rules to EMR Master

    - ![](img/6.AddEMRMasterSSHRules.png)
- Add SSH Access rule from Anywhere to EMR Master
 
    - ![](img/7.AddEMRSSHAccessFromAnywhere.png)
- Create S3 Bucket

    - ![](img/9.OmnitureBucketCreated.png)
    
- Build Maven Project

    - ![](img/10.BuildProject.png)
    
- Build Maven project creates Tar Package

    - ![](img/11.BuildCreatedTarPackage.png)
    
- Add Tar File and sample input file to S3

    - ![](img/12.AddTarPackageAndInputSampleFileToS3.png)
    
- Copy the SSH Command to SSH to EMR Master

    - ![](img/13.EMRClickOnSSHLinkToCopySSHCommand.png)
    
- SSH to EMR Master

    - ![](img/14.SSHToEMRMaster.png)
    
- Install Tar file from S3 bucket

    - ![](img/15.InstallApplicationCopyingFilesFromS3.png)
    
- Run either StartLocal.sh or StartCluster.sh

    - ![](img/16.RunEitherStartLocalOrstartCluster.png)
    
- Run startCluster.sh

    - ![](img/17.runStartCluster.png)
    
- Check output directory in S3 bucket

    - ![](img/18.CheckOutputInS3Bucket.png)
    
- Check the results

    - ![](img/19.OutputFile.png)
